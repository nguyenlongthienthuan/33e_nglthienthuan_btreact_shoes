import logo from './logo.svg';
import './App.css';
import ShoeShop from './ShoesShop/ShoeShop';

function App() {
  return (
    <div  >
       <ShoeShop></ShoeShop>
    </div>
  );
}

export default App;
