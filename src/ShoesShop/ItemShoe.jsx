import React, { Component } from 'react'

export default class ItemShoe extends Component {
  HandelShowInfo=(item)=>{
     document.querySelector(".Detailinfo").classList.toggle("DetailinfoShow");
     this.props.HandelShowInfo(item);
  }
  render() {
    return (
       <div id={'id'+this.props.Data.id} style={{position:`relative`}} className="w-100 h-100 col-xs-4 col-md-6 bootstrap snippets bootdeys">
       <div className="product-content product-wrap clearfix">
     <div className="row">
      <div className="col-md-6 col-sm-12 col-xs-12">
        <div onClick={()=>{this.HandelShowInfo(this.props.Data)}} className="product-image"> 
          <img src={this.props.Data.image} alt="194x228" className="w-100 img-responsive" /> 
        </div>
      </div>
      <div className="col-md-6 col-sm-12 col-xs-12">
        <div className="product-deatil">
          <h5 className="name">
            <a href="#">
              {this.props.Data.name} <span>quantity:{this.props.Data.quantity}</span>
            </a>
          </h5>
          <p className="price-container">
            <span>{this.props.Data.price}$</span>
          </p>
          <span className="tag1" /> 
        </div>
        <div className="description">
          <p>{this.props.Data.description}</p>
          <p>{this.props.Data.shortDescription}</p>
        </div>
      
      </div>
    </div>
    <div className="product-info smart-form">
          <div className="row">
            <div onClick={()=>{this.props.HandelAddToCart(this.props.Data)}} className="col-md-6 col-sm-6 col-xs-6"> 
              <a  className="btn btn-success">Add to cart</a>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-6">
              <div className="rating">
                <label htmlFor="stars-rating-5"><i className="fa fa-star" /></label>
                <label htmlFor="stars-rating-4"><i className="fa fa-star" /></label>
                <label htmlFor="stars-rating-3"><i className="fa fa-star text-primary" /></label>
                <label htmlFor="stars-rating-2"><i className="fa fa-star text-primary" /></label>
                <label htmlFor="stars-rating-1"><i className="fa fa-star text-primary" /></label>
              </div>
            </div>
          </div>
        </div>
  </div>
</div>

    )
  }
}
