import React, { Component } from "react";

export default class ShoppingCart extends Component {
  RenderDataBuy = () => {
    return this.props.Data.map((item) => {
      return (
        <tr>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              onClick={() => {
                this.props.ChangeQuantity(item.id, -1);
              }}
              style={{ border: `none`, background: `none` ,padding:`0 10px`}}
            >
              -
            </button>
            {item.SoLuong}
            <button
              onClick={() => {
                this.props.ChangeQuantity(item.id, 1);
              }}
              style={{ border: `none`, background: `none`,padding:`0 10px` }}
            >
              +
            </button>
          </td>
          <td>{item.price * item.SoLuong}</td>
          <td>
            <button onClick={()=>{this.props.Del(item.id)}} style={{ border: `none` }}>
              <i className="fa-solid fa-trash"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>hinh anh</th>
            <th>ten</th>
            <th>so luong</th>
            <th>gia</th>
            <th>xoa</th>
          </tr>
        </thead>
        <tbody>{this.RenderDataBuy()}</tbody>
      </table>
    );
  }
}
