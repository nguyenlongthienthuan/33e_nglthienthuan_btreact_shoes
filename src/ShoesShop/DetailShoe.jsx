import React, { Component } from 'react'

export default class DetailShoe extends Component {
  HandelShowInfo=()=>{
    document.querySelector(".Detailinfo").classList.toggle("DetailinfoShow");
 }
  render() {
    return (
      <div style={{overflow:`scroll`}} className='Detailinfo'>
        <table  className='table'>
          <thead>
            <tr>
              <th>hinh anh</th>
              <th>ten</th>
              <th>gia</th>
              <th>so luong san pham</th>
              <th>mo ta</th>
              <th>chat lieu</th>
            </tr>
          </thead>
          <tbody style={{height:`100%`}}>
            <tr>
              <td>
                <img style={{width:`100%`}} src={this.props.Data.image} alt="" />
              </td>
              <td>
                <p>{this.props.Data.name}</p>
              <p>{this.props.Data.alias}</p>
              </td>
              <td>{this.props.Data.price}$</td>
              <td>{this.props.Data.quantity}</td>
              <td>{this.props.Data.description}</td>
              <td>{this.props.Data.shortDescription}</td>
            </tr>
          </tbody>
        </table>
        <i onClick={this.HandelShowInfo} class="fa-solid fa-circle-xmark"></i>
      </div>
    )
  }
}
