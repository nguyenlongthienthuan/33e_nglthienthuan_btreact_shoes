import React, { Component } from 'react'
import { Data } from './data'
import DetailShoe from './DetailShoe'
import ItemShoe from './ItemShoe'
import ShoppingCart from './ShoppingCart'

export default class ShoeShop extends Component {
    state={
        DataBuy:[],
        ShoeInfo:"",
    }
    RenderListProduct=()=>{
        return Data.map((shoe)=> {return ( <ItemShoe Data={shoe} HandelAddToCart={this.HandelAddToCart} HandelShowInfo={this.HandelShowInfo}></ItemShoe>)})
    }
    HandelAddToCart=(shoe)=>{
      let IndexShoe=this.state.DataBuy.findIndex((item)=>{return item.id==shoe.id})
      IndexShoe==-1 && this.setState({DataBuy:[...this.state.DataBuy,{...shoe,SoLuong:1}]});
      let clone=[...this.state.DataBuy];
    //   [...this.state.DataBuy][IndexShoe].SoLuong++
      IndexShoe!=-1 && clone[IndexShoe].SoLuong++ && this.setState({DataBuy:clone});
    }
    ShowTableBuy=()=>{
      document.querySelector('.ListBuy .InSide').classList.toggle("show");
    }
    Del=(id)=>{
      let clone=[...this.state.DataBuy];
      let index=this.state.DataBuy.findIndex((item)=>{return id==item.id});
      clone.splice(index,1);
      this.setState({
        DataBuy:clone,
      })
    }
    ChangeQuantity=(id,value)=>{
      let clone=[...this.state.DataBuy];
      let index=this.state.DataBuy.findIndex((item)=>{return id==item.id});
    clone[index].SoLuong=value+clone[index].SoLuong;
     if(clone[index].SoLuong==0)clone.splice(index,1);
      this.setState({
        DataBuy:clone,
      })
    }
    TotalPrice=()=>{
      let tong=0;
       this.state.DataBuy.forEach((item)=>{tong+=item.SoLuong*item.price;});

      return tong;
    }
    HandelShowInfo=(shoe)=>{
        this.setState({
          ShoeInfo:shoe,
        })
    }
  render() {
    return (
       <>
      <div className='ListBuy'>
      <i onClick={this.ShowTableBuy}  className="fa-sharp fa-solid fa-bag-shopping"></i>
       {/* <div className='bg-table'><ShoppingCart Data={this.state.DataBuy} Del={this.Del}></ShoppingCart></div> */}
       <div className='InSide'>
             <div className='price'><span>{this.TotalPrice()}$</span></div>
             <div className='SoLuong'><span>0</span></div>
             <div className='type'> <span> type:{this.state.DataBuy.length}</span></div>
             <div className='bg-table'><ShoppingCart Data={this.state.DataBuy} Del={this.Del} ChangeQuantity={this.ChangeQuantity}></ShoppingCart></div>
        </div>
      </div>
       <div className='container'>
        <div className='row'>
        {this.RenderListProduct()}
        </div>
      </div>
      <div className='container'><DetailShoe Data={this.state.ShoeInfo}></DetailShoe></div>
      </>
    )
  }
}
